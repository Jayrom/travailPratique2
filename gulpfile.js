/**
 * Created by Jérôme Cloutier on 10/08/2016.
 */
'use strict';

var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');

// Tâche Style
gulp.task('style', function() {
   return gulp.src('src/scss/**/*.scss')
      .pipe(sourcemaps.init())
      .pipe(sass().on('error', sass.logError))
      .pipe(autoprefixer({
         browsers: ['last 2 versions']
      }))
      .pipe(csso())
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('dist'));
});

gulp.task('default', function(){
   gulp.src('src/**/*.html')
      .pipe(gulp.dest('dist'));
});